Crafty.c("tut",{
		init: function(){
		Crafty.viewport.x = 0;
		Crafty.viewport.y = 0;
		var bg = Crafty.e("2D, Canvas, Color")
				.color("#FFF")
				.attr({w:gamewidth,h:gamehight,x:0,y:0,z:1000}); 
		this.addComponent("2D, DOM, Text, Keyboard")
		.attr({w: gamewidth, h:20, x:0, y:gamehight/2 - 10})
    	.css({"text-align": "center"});
    	this.bind('KeyDown', function (key) {
		if (key.key == 32){
				bg.destroy();
				this.destroy();
				playaudio = true;
				Crafty.e("player").attr({x:playerx, y:playery});
			if (lvl == 15){
				lvl = 0;
				Crafty.scene("go");
			}
		}});
	}
			
})