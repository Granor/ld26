Crafty.c("elevator",{
	init: function(){
	var flvl = lvl;
		this.timeout(function(){
			if(flvl == lvl){
			Crafty.e("elevator").attr({x:this.x,y:this.y});
			Crafty.e("elevate").attr({x:this.x,y:this.y});
			this.destroy();
			}
		}, 1000)
		}
})


Crafty.c("elevate",{
	init: function(){
		var played = false;
		this.addComponent("2D, Canvas, Color, Collision")
			.color("#000")
			.attr({w:32,h:32});
		console.log("elevator");
		this.bind("EnterFrame", function(){
			this.move("n", 3);
			if (this.hit("player")){
				if (!played){
				Crafty.audio.play("elevator", 1);		
			    played = true;}
			    
			}
			if (this.hit("destelv")){
				this.destroy();
			}
		})
	}
})

Crafty.c("destelv",{
		init: function(){
		this.addComponent("2D, Canvas, Color");
		this.color("#333")
			.attr({w:32, h:32});
	}
})
