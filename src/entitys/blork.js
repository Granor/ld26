Crafty.c("blork",{
	init: function(){
		var flvl = lvl;
		this.addComponent("2D, Canvas, Color")
			.color("#333")
			.attr({w:32,h:32});
		this.timeout(function(){
			if(flvl == lvl){
			if (playaudio){
			Crafty.audio.play("blork");
			}
			for(var i = 0; i < 5; i++){
				console.log(i);
				this.timeout(function(){
					console.log("shoot");
					Crafty.e("shoot").attr({x:this.x,y:this.y});
				}, i*100)
			}
			Crafty.e("blork").attr({x:this.x,y:this.y});
		 	}
			this.destroy();
		}, 3000)
	}
})

Crafty.c("shoot",{
	init: function(){
		this.addComponent("2D, Canvas, Color, Collision, enemy")
			.attr({h:32,w:32})
			.color("#AAA");
		this.bind("EnterFrame",function(){
			this.move("w", 5);
		if (this.hit("blorkdest")){
			this.destroy();
		}
		})
	    this.timeout(function(){
			this.destroy();
		}, 10000)
	}
});

Crafty.c("blorkdest",{
	init: function(){
		this.addComponent("2D,Canvas,Color,Collision")
			.color("#333")
			.attr({h:32,w:32});
	}
})
