Crafty.c("laser",{
	init: function(){
		var flvl = lvl;
		this.addComponent("2D, Canvas, Color, Collision");
		this.color("#999")
			.attr({w:32, h:32});
		this.timeout(function(){
			if (flvl == lvl){
			Crafty.e("laser").attr({x:this.x,y:this.y});
			Crafty.e("reallaser").attr({x:this.x+16,y:this.y});
			}
			this.destroy();
		}, 1000)
	}
})

Crafty.c("reallaser",{
		init: function(){
			if (playaudio){
		Crafty.audio.play("laser", 1);}
		this.addComponent("2D, Canvas, Color, Collision, enemy");
		this.color("#999")
			.attr({w:2, h:1000});
		this.timeout(function(){
			this.destroy();
		}, 50)
	}
})
