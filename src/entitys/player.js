  	Crafty.c("player",{
  		init: function(){
  			this.addComponent("2D, Canvas, Multiway, Color, Gravity, Collision");
			this.color("#666")
				.attr({w:25,h:25})
				.multiway(3, {SPACE: 0})
				.gravity("floor");
				
			//this.collision(new Crafty.polygon([0,0], [0,25], [25,25], [25,0]));
			this.bind("EnterFrame", function(){
				Crafty.viewport.x = -this.x + Crafty.viewport.width / 2;
      			Crafty.viewport.y = -this.y + Crafty.viewport.height / 2;
      			if (this.hit("enemy")){
      				console.log("lose");
      				this.destroy();
      				Crafty.e("lose");
      			}
      			if (this.hit("elevate")){
      				this._falling = false;
      				console.log("elevate");
      				this.move("n", 3)
      			}
      			if (this.hit("finish")){
      				respawn = false;
      				Crafty.scene("win");
      			}
			})		  
		  
			}
  	});