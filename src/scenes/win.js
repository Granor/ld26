Crafty.scene("win", function() {
		Crafty.viewport.x = 0;
		Crafty.viewport.y = 0;
		Crafty.audio.play("win",1);
		lvl++;
		var bg = Crafty.e("2D, Canvas, Color")
						.color("#FFF")
						.attr({w:gamewidth,h:gamehight,x:0,y:0,z:1000});
	  	var text = Crafty.e("2D, DOM, Text, Keyboard").attr({w: gamewidth, h:20, x:0, y:gamehight/2 - 10,z:1001})
    	.text("You won")
    	.css({"text-align": "center"})
    	.text("You won<br>press Space to continue.")
		.bind('KeyDown', function (key) {
			if (key.key == 32){
					Crafty.scene("start");
				}});
 
	});
