Crafty.scene("lose", function() {
	    respawn = false;
		Crafty.viewport.x = 0;
		Crafty.viewport.y = 0;
		Crafty.audio.play("lose",1);
	  	var text = Crafty.e("2D, DOM, Text").attr({w: gamewidth, h:20, x:0, y:gamehight/2 - 10})
    	.text("You lost")
    	.css({"text-align": "center"});
    	text.timeout(function(){
			text.text("You lost<br><br>Press Space to play again.")
			     .addComponent('Keyboard').bind('KeyDown', function (key) {
			if (key.key == 32){
					Crafty.scene("start");
				}});
		}, 3000)
 
	});

Crafty.c("lose", {
	init: function(){
		Crafty.audio.play("lose",1);
		playaudio = false;
		Crafty.viewport.x = 0;
		Crafty.viewport.y = 0;
		var bg = Crafty.e("2D, Canvas, Color")
						.color("#FFF")
						.attr({w:gamewidth,h:gamehight,x:0,y:0,z:1000});
		var text = Crafty.e("2D, DOM, Text, Keyboard").attr({w: gamewidth, h:20, x:0, y:gamehight/2 - 10,z:1001})
    					.text("You lost<br><br>Press Space to play again.")
    					.css({"text-align": "center"})
    					.bind('KeyDown', function (key) {
							if (key.key == 32){
								text.destroy();
								bg.destroy();
								playaudio = true;
								Crafty.e("player").attr({x:playerx,y:playery});
				}});;
	}
})